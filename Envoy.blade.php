@servers(['web' => $user.'@'.$host,'localhost' => '127.0.0.1'])

{{-- stories --}}

@story('deploy')
    validate_cli_params
    check_frontend_changes
    build_and_upload_frontend
    pull_git_changes
    install_composer_dependencies
    run_db_migrations
    deploy_frontend
@endstory

{{-- tasks --}}

@task('validate_cli_params', ['on' => 'localhost'])
    @if(empty($host) || empty($user) || empty($project_path))
        echo "Usage envoy run deploy --host=HOST --user=USER --project_path=PATH"
        exit 1
    @else
        echo "Running with environment:"
        echo "host = {{ $host }}"
        echo "user = {{ $user }}"
        echo "project_path = {{ $project_path }}"
    @endif
@endtask

@task('check_frontend_changes', ['on' => 'localhost'])
    @php
        $lock_file = 'resources.lock';
        $prev_hash = file_exists($lock_file) ? file_get_contents($lock_file) : 'empty';
        $actual_hash = exec('git log -n 1 --format="%H" -- resources');
    @endphp
    echo "Previous resources hash {{ $prev_hash }}"
    echo "Actual resources hash {{ $actual_hash }}"
@endtask

@task('build_and_upload_frontend', ['on' => 'localhost'])
    @php
        $archive = 'frontend.tar.gz';
    @endphp
    @if ($actual_hash !== $prev_hash)
        npm install
        npm run prod

        cd public/
        tar -czvf {{ $archive }} js css mix-manifest.json
        scp {{ $archive }} {{ "$user@$host:~/$archive" }}
        rm -f {{ $archive }}
        cd ..

        php -r 'file_put_contents("{{ $lock_file }}", "{{ $actual_hash }}");'
        git add {{ $lock_file }}
        git commit -m "Frontend build for resource hash {{ $actual_hash }}"
        git push
    @else
        echo 'No need to rebuild'
    @endif
@endtask

@task('pull_git_changes', ['on' => 'web'])
    cd {{ $project_path }}
    git fetch
    git status
    git pull origin master
@endtask

@task('install_composer_dependencies', ['on' => 'web'])
    cd {{ $project_path }}
    composer install
@endtask

@task('run_db_migrations', ['on' => 'web'])
    cd {{ $project_path }}
    php artisan migrate:status
    php artisan migrate --step --force
@endtask

@task('deploy_frontend', ['on' => 'web'])
    @if ($actual_hash !== $prev_hash)
        rm -rf {{ "$project_path/public/js" }}
        rm -rf {{ "$project_path/public/css" }}
        rm -f {{ "$project_path/mix-manifest.json" }}

        tar -xzvf {{ $archive }} -C {{ "$project_path/public/" }}
        rm -f {{ $archive }}
    @else
        echo 'No need to deploy'
    @endif
@endtask
